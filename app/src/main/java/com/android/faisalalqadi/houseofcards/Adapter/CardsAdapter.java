package com.android.faisalalqadi.houseofcards.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.faisalalqadi.houseofcards.Model.Card;
import com.android.faisalalqadi.houseofcards.Model.CardSet;
import com.android.faisalalqadi.houseofcards.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Fai on 2014-08-18.
 */
public class CardsAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private Card[] cards;

    public CardsAdapter(Context context, Card[] cards) {
        mInflater = LayoutInflater.from(context);
        this.cards = cards;
    }

    @Override
    public int getCount() {
        return cards.length;
    }

    @Override
    public Card getItem(int i) {
        return cards[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        CardHolder holder;
        Card card = getItem(i);

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.layout_card, viewGroup, false);
            holder = new CardHolder(convertView);
            convertView.setTag(holder);
        }
        else
            holder = (CardHolder) convertView.getTag();

        holder.cardTitle.setText(card.getTitle());
        holder.cardFaction.setText(card.getFaction());
        holder.cardSet.setText(card.getSetname());
        holder.cardType.setText(card.getType() + ": " + card.getSubtype());
        holder.cardText.setText(card.getText());
        return convertView;
    }

    static class CardHolder{
        @InjectView(R.id.card_title) TextView cardTitle;
        @InjectView(R.id.card_faction) TextView cardFaction;
        @InjectView(R.id.card_set) TextView cardSet;
        @InjectView(R.id.card_type) TextView cardType;
        @InjectView(R.id.card_text) TextView cardText;
        private CardHolder(View view){ ButterKnife.inject(this, view); }
    }
}
