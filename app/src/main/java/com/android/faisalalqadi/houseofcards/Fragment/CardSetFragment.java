package com.android.faisalalqadi.houseofcards.Fragment;



import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;

import com.android.faisalalqadi.houseofcards.Adapter.CardsAdapter;
import com.android.faisalalqadi.houseofcards.Interface.OnCardClickedListener;
import com.android.faisalalqadi.houseofcards.Interface.OnCardSetClickedListener;
import com.android.faisalalqadi.houseofcards.Model.Card;
import com.android.faisalalqadi.houseofcards.R;
import com.android.faisalalqadi.houseofcards.Request.GetRequest;
import com.android.faisalalqadi.houseofcards.Utility;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CardSetFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class CardSetFragment extends BaseFragment implements PopupMenu.OnMenuItemClickListener, RequestListener<Card[]> {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SET_CODE = "set-code";

    private String mCardSetCode;
    private OnCardClickedListener mCallback;
    public static final String FRAG_TAG = CardSetFragment.class.getSimpleName();

    @InjectView(R.id.card_list)
    ListView cardsListView;
    @InjectView(R.id.network_progress)
    ProgressBar progressBar;

    private String CACHE_KEY;
    private Card[] cards;
    private CardsAdapter mAdapter;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param setCode code of the card set to display.
     * @return A new instance of fragment CardSetFragment.
     */
    public static CardSetFragment newInstance(String setCode) {
        CardSetFragment fragment = new CardSetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SET_CODE, setCode);
        fragment.setArguments(args);
        return fragment;
    }
    public CardSetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCardSetCode = getArguments().getString(ARG_SET_CODE);
            CACHE_KEY = Utility.buildCardSetCacheKey(mCardSetCode);
        }

        setHasOptionsMenu(true);

    }

    @OnItemClick(R.id.card_list)
    public void showCard(int position){
        mCallback.onCardClicked(cards[position]);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_card_set, container, false);
        ButterKnife.inject(this, view);
        makeNetworkCall(new GetRequest<Card[]>(Utility.buildSetEndpoint(mCardSetCode), Card[].class), CACHE_KEY, DurationInMillis.ONE_HOUR, this);
        showProgress(true, progressBar, cardsListView);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.card_set_menu, menu);
        ImageView item = (ImageView) menu.findItem(R.id.action_sort).getActionView();
        item.setImageDrawable(new IconDrawable(getActivity(), Iconify.IconValue.fa_filter)
                .colorRes(android.R.color.secondary_text_dark)
                .actionBarSize());
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSortMenu(view);
            }
        });

    }

    private void showSortMenu(View view){
        PopupMenu popup = new PopupMenu(getActivity(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.sort_submenu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        if(cards.length == 0)
            return true;
        int id = menuItem.getItemId();
        if(id == R.id.sort_faction)
            updateListContents(Utility.sortByFaction(cards));
        else if(id == R.id.sort_type)
            updateListContents(Utility.sortByType(cards));
        else if(id == R.id.sort_number)
            updateListContents(Utility.sortByNumber(cards));
        else if(id == R.id.sort_side)
            updateListContents(Utility.sortBySide(cards));
        else
            updateListContents(Utility.sortByTitle(cards));

        return true;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        showProgress(false, progressBar, cardsListView);
    }

    @Override
    public void onRequestSuccess(Card[] cards) {
        showProgress(false, progressBar, cardsListView);
        getSpiceManager().putInCache(Card[].class, CACHE_KEY, cards);
        updateListContents(cards);
    }

    private void updateListContents(Card[] cards){
        this.cards = cards;
        mAdapter = new CardsAdapter(getActivity(), cards);
        cardsListView.setAdapter(mAdapter);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnCardClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

}
