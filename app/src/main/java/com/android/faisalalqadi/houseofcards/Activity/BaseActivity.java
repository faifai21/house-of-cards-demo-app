package com.android.faisalalqadi.houseofcards.Activity;

import android.support.v4.app.FragmentActivity;

import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.spicelist.okhttp.OkHttpBitmapSpiceManager;

/**
 * Created by Fai on 2014-08-11.
 */
public class BaseActivity extends FragmentActivity {

    protected SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);
    protected OkHttpBitmapSpiceManager bitmapSpiceManager = new OkHttpBitmapSpiceManager();

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
        bitmapSpiceManager.start(this);
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        bitmapSpiceManager.shouldStop();
        super.onStop();
    }

    public SpiceManager getSpiceManager(){
        return spiceManager;
    }

    public OkHttpBitmapSpiceManager getBitmapManager(){
        return bitmapSpiceManager;
    }
}
