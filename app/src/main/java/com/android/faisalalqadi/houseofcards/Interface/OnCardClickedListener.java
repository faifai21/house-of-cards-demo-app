package com.android.faisalalqadi.houseofcards.Interface;

import com.android.faisalalqadi.houseofcards.Model.Card;

/**
 * Created by Fai on 2014-08-19.
 */
public interface OnCardClickedListener {
    public void onCardClicked(Card card);
}
