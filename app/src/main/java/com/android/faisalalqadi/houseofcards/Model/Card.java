package com.android.faisalalqadi.houseofcards.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.android.faisalalqadi.houseofcards.SortBy;
import com.android.faisalalqadi.houseofcards.Utility;

/**
 * Created by Fai on 2014-08-18.
 */
public class Card implements Comparable<Card>, Parcelable{
    private String code;
    private String title;
    private String type;
    private String subtype;
    private String faction;
    private Integer number;
    private String text;
    private String side;
    private String setname;
    private String imagesrc;
    private String largeimagesrc;

    private static SortBy SORT_BY = SortBy.TITLE;

    public static void sortBy(SortBy sort){
        SORT_BY = sort;
    }

    public static SortBy getSortBy(){
        return SORT_BY;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getImagesrc() {
        return  Utility.URL + imagesrc;
    }

    public void setImagesrc(String imagesrc) {
        this.imagesrc = imagesrc;
    }

    public String getLargeimagesrc() {
        return  Utility.URL + largeimagesrc;
    }

    public void setLargeimagesrc(String largeimagesrc) {
        this.largeimagesrc = largeimagesrc;
    }

    public String getSetname() {
        return setname;
    }

    public void setSetname(String setname) {
        this.setname = setname;
    }

    @Override
    public int compareTo(Card card) {
        // All sorts are ascending order
        switch (Card.SORT_BY){
            // Sort by Type
            case TYPE:
                return type.compareTo(card.getType());
            // Sort by Side
            case SIDE:
                return side.compareTo(card.getSide());
            // Sort By Faction
            case FACTION:
                return faction.compareTo(card.getFaction());
            // Sort By Number
            case NUMBER:
                return number.compareTo(card.getNumber());
            // Sort By Title
            default:
                return title.compareTo(card.title);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.title);
        dest.writeString(this.type);
        dest.writeString(this.subtype);
        dest.writeString(this.faction);
        dest.writeValue(this.number);
        dest.writeString(this.text);
        dest.writeString(this.side);
        dest.writeString(this.setname);
        dest.writeString(this.imagesrc);
        dest.writeString(this.largeimagesrc);
    }

    public Card() {
    }

    private Card(Parcel in) {
        this.code = in.readString();
        this.title = in.readString();
        this.type = in.readString();
        this.subtype = in.readString();
        this.faction = in.readString();
        this.number = (Integer) in.readValue(Integer.class.getClassLoader());
        this.text = in.readString();
        this.side = in.readString();
        this.setname = in.readString();
        this.imagesrc = in.readString();
        this.largeimagesrc = in.readString();
    }

    public static final Creator<Card> CREATOR = new Creator<Card>() {
        public Card createFromParcel(Parcel source) {
            return new Card(source);
        }

        public Card[] newArray(int size) {
            return new Card[size];
        }
    };
}
