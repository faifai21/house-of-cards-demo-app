package com.android.faisalalqadi.houseofcards;

/**
 * Created by Fai on 2014-08-20.
 */
public enum SortBy {
    TITLE,
    TYPE,
    FACTION,
    NUMBER,
    SIDE
}
