package com.android.faisalalqadi.houseofcards.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.android.faisalalqadi.houseofcards.Fragment.CardFragment;
import com.android.faisalalqadi.houseofcards.Fragment.CardSetFragment;
import com.android.faisalalqadi.houseofcards.Fragment.CardSetListFragment;
import com.android.faisalalqadi.houseofcards.Interface.OnCardClickedListener;
import com.android.faisalalqadi.houseofcards.Interface.OnCardSetClickedListener;
import com.android.faisalalqadi.houseofcards.Model.Card;
import com.android.faisalalqadi.houseofcards.R;


public class MainActivity extends BaseActivity implements OnCardSetClickedListener, OnCardClickedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, CardSetListFragment.newInstance())
                    .commit();
        }
    }


    private void swapFragment(Fragment fragment, String tag){
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.container, fragment)
                .addToBackStack(tag)
                .commit();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCardClicked(String code) {
        swapFragment(CardSetFragment.newInstance(code), CardSetFragment.FRAG_TAG);
    }

    @Override
    public void onCardClicked(Card card) {
        swapFragment(CardFragment.newInstance(card), CardFragment.FRAG_TAG);
    }

}
