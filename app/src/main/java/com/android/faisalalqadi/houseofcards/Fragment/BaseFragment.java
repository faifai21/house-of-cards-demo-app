package com.android.faisalalqadi.houseofcards.Fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import com.android.faisalalqadi.houseofcards.Activity.BaseActivity;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.spicelist.okhttp.OkHttpBitmapSpiceManager;

/**
 * Created by Fai on 2014-08-18.
 */
public class BaseFragment extends Fragment {

    protected final SpiceManager getSpiceManager(){
        if(getActivity() instanceof BaseActivity)
            return ((BaseActivity)getActivity()).getSpiceManager();
        else
            return null;
    }

    protected final OkHttpBitmapSpiceManager getBitmapManager(){
        if(getActivity() instanceof BaseActivity)
            return ((BaseActivity)getActivity()).getBitmapManager();
        else
            return null;
    }

    protected final void makeNetworkCall(SpiceRequest request, RequestListener listener){
        getSpiceManager().execute(request, listener);
    }

    protected final void makeNetworkCall(SpiceRequest request, Object cache_key, long cache_duration, RequestListener listener){
        getSpiceManager().getFromCacheAndLoadFromNetworkIfExpired(request, cache_key, cache_duration, listener);
    }

    protected final void showProgress(final boolean show, final ProgressBar mProgressView, final View mPrimaryView) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mPrimaryView.setVisibility(show ? View.GONE : View.VISIBLE);
            mPrimaryView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mPrimaryView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
        else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mPrimaryView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
