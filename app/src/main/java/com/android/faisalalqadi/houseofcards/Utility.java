package com.android.faisalalqadi.houseofcards;

import com.android.faisalalqadi.houseofcards.Model.Card;
import com.squareup.okhttp.internal.Util;

import java.util.Arrays;

/**
 * Created by Fai on 2014-08-18.
 */
public class Utility {
    /*
     *  URL and API endpoint generation section
     *
     */
    public static final String URL = "http://netrunnerdb.com";
    public static final String BASE_URL = URL + "/api/";
    private static final String SET_APPEND = BASE_URL + "set";
    public static final String SETS_ENDPOINT = SET_APPEND + "s/";
    public static final String SET_URL = SET_APPEND + "/";
    public static final String CARD_URL = BASE_URL + "card/";
    public static final String CARDS_URL = BASE_URL + "cards/";

    public static String buildSetEndpoint(String setCode){
        return SET_URL + setCode;
    }

    public static String buildCardEndpoint(String cardCode){
        return CARD_URL + cardCode;
    }

    /*
     *  Sorting section. Pass in an array of cards to a sort function to sort by that type.
     *  These ints are used in the comparator implementation of the Card class
     *  These ints should perhaps be an enum.
     *  Ints have been moved to enums: found in the SortBy enum file.
     *  Will keep ints here for now
     */
    public static final int SORT_TITLE = 0;
    public static final int SORT_TYPE = 1;
    public static final int SORT_FACTION = 2;
    public static final int SORT_NUMBER = 3;
    public static final int SORT_SIDE = 4;

    public static Card[] sortBy(Card[] cards, SortBy sortBy){
        Card.sortBy(sortBy);
        Arrays.sort(cards);
        return cards;
    }

    public static Card[] sortByTitle(Card[] cards){
        return sortBy(cards, SortBy.TITLE);
    }

    public static Card[] sortByType(Card[] cards){
        return sortBy(cards, SortBy.TYPE);
    }

    public static Card[] sortByNumber(Card[] cards){
        return sortBy(cards, SortBy.NUMBER);
    }

    public static Card[] sortByFaction(Card[] cards){
        return sortBy(cards, SortBy.FACTION);
    }

    public static Card[] sortBySide(Card[] cards){
        return sortBy(cards, SortBy.SIDE);
    }

    /*
     *  Cache Key generation section
     *
     */
    public static final String CARD_SETS_CACHE_KEY = "card-sets-cache";
    private static final String CARD_SET_CACHE_KEY = "-card-set-cache";

    public static String buildCardSetCacheKey(String setCode){
        return setCode + CARD_SET_CACHE_KEY;
    }


    /*
     *  Bundle Keys sections
     */

    public static final String LIST_VIEW_POSITION_KEY = "list-view-pos";


}
