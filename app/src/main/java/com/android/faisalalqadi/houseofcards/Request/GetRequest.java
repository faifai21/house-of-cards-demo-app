package com.android.faisalalqadi.houseofcards.Request;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

/**
 * Created by Fai on 2014-08-18.
 */
public class GetRequest<T> extends SpringAndroidSpiceRequest<T> {

    private String url;
    private Class<T> clazz;
    public GetRequest(String url, Class<T> clazz) {
        super(clazz);
        this.clazz = clazz;
        this.url = url;
    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        return getRestTemplate().getForObject(url, clazz);
    }
}
