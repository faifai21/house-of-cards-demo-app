package com.android.faisalalqadi.houseofcards.Fragment;



import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.faisalalqadi.houseofcards.Model.Card;
import com.android.faisalalqadi.houseofcards.R;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CardFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class CardFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_CARD = "card-arg";
    public static final String FRAG_TAG = CardFragment.class.getSimpleName();
    private Card mCard;
    @InjectView(R.id.card_image_src)
    ImageView imageSrc;
    @InjectView(R.id.fragment_card_faction)
    TextView cardFaction;
    @InjectView(R.id.fragment_card_number)
    TextView cardNumber;
    @InjectView(R.id.fragment_card_side)
    TextView cardSide;
    @InjectView(R.id.fragment_card_subtype)
    TextView cardSubType;
    @InjectView(R.id.fragment_card_text)
    TextView cardText;
    @InjectView(R.id.fragment_card_title)
    TextView cardTitle;
    @InjectView(R.id.fragment_card_type)
    TextView cardType;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param card card to display.
     * @return A new instance of fragment CardFragment.
     */
    public static CardFragment newInstance(Card card) {
        CardFragment fragment = new CardFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CARD, card);
        fragment.setArguments(args);
        return fragment;
    }
    public CardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCard = getArguments().getParcelable(ARG_CARD);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_card, container, false);
        ButterKnife.inject(this, view);
        Picasso.with(getActivity())
                .load(mCard.getLargeimagesrc() != null ? mCard.getLargeimagesrc() : mCard.getImagesrc())
                .resize(900, 1200)
                .placeholder(R.drawable.ic_launcher)
                .into(imageSrc);
        cardFaction.setText("Faction: " + mCard.getFaction());
        cardNumber.setText("Number: " + mCard.getNumber());
        cardSide.setText("Side: " + mCard.getSide());
        cardSubType.setText("Subtype: " + mCard.getSubtype());
        cardText.setText(mCard.getText());
        cardTitle.setText(mCard.getTitle());
        cardType.setText("Type: " + mCard.getType());
        return view;
    }


}
