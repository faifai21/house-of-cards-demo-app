package com.android.faisalalqadi.houseofcards.Interface;

/**
 * Created by Fai on 2014-08-18.
 */
public interface OnCardSetClickedListener {
    public void onCardClicked(String code);
}
