package com.android.faisalalqadi.houseofcards.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.faisalalqadi.houseofcards.Model.CardSet;
import com.android.faisalalqadi.houseofcards.R;

import org.w3c.dom.Text;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Fai on 2014-08-18.
 */
public class CardSetsAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private CardSet[] cardSets;
    public CardSetsAdapter(Context context, CardSet[] cardSets) {
        mInflater = LayoutInflater.from(context);
        this.cardSets = cardSets;
    }

    @Override
    public int getCount() {
        return cardSets.length;
    }

    @Override
    public CardSet getItem(int i) {
        return cardSets[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        CardSetHolder holder;
        CardSet cardSet = getItem(i);

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.layout_card_set, viewGroup, false);
            holder = new CardSetHolder(convertView);
            convertView.setTag(holder);
        }
        else
            holder = (CardSetHolder) convertView.getTag();

        holder.knownText.setText("Known Cards: " + cardSet.getKnown());
        holder.titleText.setText(cardSet.getName());
        holder.totalText.setText("Total Cards: " + cardSet.getTotal());

        return convertView;
    }

    static class CardSetHolder {
        @InjectView(R.id.card_set_known)
        TextView knownText;
        @InjectView(R.id.card_set_title)
        TextView titleText;
        @InjectView(R.id.card_set_total)
        TextView totalText;

        private CardSetHolder(View view){
            ButterKnife.inject(this, view);
        }
    }
}
