package com.android.faisalalqadi.houseofcards.Fragment;



import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.faisalalqadi.houseofcards.Adapter.CardSetsAdapter;
import com.android.faisalalqadi.houseofcards.Interface.OnCardSetClickedListener;
import com.android.faisalalqadi.houseofcards.Model.CardSet;
import com.android.faisalalqadi.houseofcards.R;
import com.android.faisalalqadi.houseofcards.Request.GetRequest;
import com.android.faisalalqadi.houseofcards.Utility;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CardSetListFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class CardSetListFragment extends BaseFragment implements RequestListener<CardSet[]> {

    @InjectView(R.id.card_set_list)
    ListView cardSetList;

    @InjectView(R.id.network_progress)
    ProgressBar progressBar;

    private CardSetsAdapter mAdapter;
    private CardSet[] cardSets;

    private OnCardSetClickedListener mCallback;

    public static final String FRAG_TAG = CardSetListFragment.class.getSimpleName();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CardSetListFragment.
     */
    public static CardSetListFragment newInstance() {
        CardSetListFragment fragment = new CardSetListFragment();
        return fragment;
    }
    public CardSetListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_card_set_list, container, false);
        ButterKnife.inject(this, view);
        makeNetworkCall(new GetRequest<CardSet[]>(Utility.SETS_ENDPOINT, CardSet[].class), Utility.CARD_SETS_CACHE_KEY, DurationInMillis.ONE_HOUR, this);
        showProgress(true, progressBar, cardSetList);
        return view;
    }

    @OnItemClick(R.id.card_set_list)
    public void showCardSet(int position){
        CardSet cardSet = cardSets[position];
        mCallback.onCardClicked(cardSet.getCode());
    }

    private void updateListContents(CardSet[] sets){
        mAdapter = new CardSetsAdapter(getActivity(), sets);
        cardSetList.setAdapter(mAdapter);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        spiceException.printStackTrace();
        Toast.makeText(getActivity(), "Failed to make network call: " + spiceException.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        showProgress(false, progressBar, cardSetList);
    }

    @Override
    public void onRequestSuccess(CardSet[] cardSets) {
        this.cardSets = cardSets;
        getSpiceManager().putInCache(CardSet[].class, Utility.CARD_SETS_CACHE_KEY, cardSets);
        updateListContents(cardSets);
        showProgress(false, progressBar, cardSetList);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnCardSetClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }
}
